[tox]
# Don't cover Python 3.8 since this is just a shim for that version.  Do at
# least make sure we don't regress!
envlist = {py27,py34,py35,py36,py37}-{nocov,cov,diffcov},py38-nocov,qa,docs
skip_missing_interpreters = True


[testenv]
commands =
    nocov: python -m unittest discover
    cov,diffcov: python -m coverage run {[coverage]rc} -m unittest discover {posargs}
    cov,diffcov: python -m coverage combine {[coverage]rc}
    cov: python -m coverage html {[coverage]rc}
    cov: python -m coverage xml {[coverage]rc}
    cov: python -m coverage report -m {[coverage]rc} --fail-under=100
    diffcov: python -m coverage xml {[coverage]rc}
    diffcov: diff-cover coverage.xml --html-report diffcov.html
    diffcov: diff-cover coverage.xml --fail-under=100
usedevelop = True
passenv =
    PYTHON*
    LANG*
    LC_*
    PYV
deps =
     cov,diffcov: coverage>=4.5
     diffcov: diff_cover
     importlib_resources
     py27: contextlib2
     pip >= 18
     packaging
setenv =
    cov: COVERAGE_PROCESS_START={[coverage]rcfile}
    cov: COVERAGE_OPTIONS="-p"
    cov: COVERAGE_FILE={toxinidir}/.coverage
    py27: PYV=2
    py34,py35,py36,py37,py38: PYV=3
    # workaround deprecation warnings in pip's vendored packages
    PYTHONWARNINGS=ignore:Using or importing the ABCs:DeprecationWarning:pip._vendor


[testenv:qa]
basepython = python3.7
commands =
    python -m flake8 importlib_metadata
    mypy importlib_metadata
deps =
    mypy
    flake8
    flufl.flake8


[testenv:docs]
basepython = python3
commands =
    sphinx-build importlib_metadata/docs build/sphinx/html
extras =
    docs


[testenv:release]
basepython = python3
deps =
    twine
    wheel
    setuptools
    keyring
    setuptools_scm
passenv =
    TWINE_USERNAME
    TWINE_PASSWORD
commands =
    python setup.py sdist bdist_wheel
    python -m twine {posargs} upload dist/*


[coverage]
rcfile = {toxinidir}/coverage.ini
rc = --rcfile={[coverage]rcfile}


[flake8]
hang-closing = True
jobs = 1
max-line-length = 79
exclude =
    # Exclude the entire top-level __init__.py file since its only purpose is
    # to expose the version string and to handle Python 2/3 compatibility.
    importlib_metadata/__init__.py
    importlib_metadata/docs/conf.py
enable-extensions = U4
